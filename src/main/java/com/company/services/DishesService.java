package com.company.services;

import com.company.entity.DishesEntity;
import com.company.domains.Dishes;
import com.company.dto.AppErrorDTO;
import com.company.dto.DataDTO;
import com.company.dto.ResponseEntity;
import com.company.exeptions.GenericNotFoundException;

import java.util.Comparator;
import java.util.List;

public class DishesService implements BaseService<Dishes> {

    private final DishesEntity dishesDao = new DishesEntity();

    public ResponseEntity<DataDTO<Dishes>> findByName(String name) {
        try {
            Dishes dishes = dishesDao.findAll().stream().filter(dish ->
                    dish.getName().equals(name)).findFirst().orElse(null);
            if (dishes == null) {
                throw new GenericNotFoundException("Dishes not found!");
            }
            return new ResponseEntity<>(new DataDTO<>(dishes), 200);
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public ResponseEntity<DataDTO<List<Dishes>>> findAll(String sort) {
        try {
            List<Dishes> dishesList = dishesDao.findAll();
            if (dishesList.isEmpty()) {
                throw new GenericNotFoundException("Dishes not found!");
            }
            switch (sort) {
                case "1" -> dishesList.sort(Comparator.comparing(Dishes::getId));
                case "2" -> dishesList.sort(Comparator.comparing(Dishes::getName));
                case "3" -> dishesList.sort(Comparator.comparing(Dishes::getPrice));
                case "4" -> dishesList.sort(Comparator.comparing(Dishes::getQuantity));
            }
            return new ResponseEntity<>(new DataDTO<>(dishesList));
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public ResponseEntity<DataDTO<Dishes>> findByID(Long id) {
        try {
            Dishes dishes = dishesDao.findAll().stream().filter(dish -> dish.getId().equals(id)).findFirst().orElse(null);
            if (dishes == null) {
                throw new GenericNotFoundException("Dishes not found!");
            }
            return new ResponseEntity<>(new DataDTO<>(dishes), 200);
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public ResponseEntity<DataDTO<List<Dishes>>> findByPrice(Double price) {
        try {
            List<Dishes> dishes = dishesDao.findAll().stream().filter(dish -> dish.getPrice().equals(price)).toList();
            if (dishes.isEmpty()) {
                throw new GenericNotFoundException("Dishes not found!");
            }
            return new ResponseEntity<>(new DataDTO<>(dishes), 200);
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public ResponseEntity<DataDTO<List<Dishes>>> findByQuantity(Integer quantity) {
        try {
            List<Dishes> dishes = dishesDao.findAll().stream().filter(dish -> dish.getQuantity().equals(quantity)).toList();
            if (dishes.isEmpty()) {
                throw new GenericNotFoundException("Dishes not found!");
            }
            return new ResponseEntity<>(new DataDTO<>(dishes), 200);
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public ResponseEntity<DataDTO<List<Dishes>>> filterByPrice(Double min, Double max) {
        try {
            List<Dishes> dishes = dishesDao.findAll().stream().filter(dish ->
                    dish.getPrice() >= min && dish.getPrice() <= max).toList();
            if (dishes.isEmpty()) {
                throw new GenericNotFoundException("Dishes not found!");
            }
            return new ResponseEntity<>(new DataDTO<>(dishes), 200);
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }
}
