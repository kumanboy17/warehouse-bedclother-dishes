package com.company.entity;

import com.company.config.FileReader;
import com.company.domains.BedClothes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BedClothesEntity implements BaseEntity<BedClothes> {

    private final String bedClothesFile = "src/main/resources/bedclothes.csv";

    private final FileReader fileReader = new FileReader();

    @Override
    public List<BedClothes> findAll() throws IOException {
        return readBedClothesFile();
    }

    public List<BedClothes> readBedClothesFile() throws IOException {
        List<BedClothes> bedClothes = new ArrayList<>();
        List<String> strings = fileReader.readFile(bedClothesFile);
        strings.forEach(s -> bedClothes.add(toBedClothes(s)));
        return bedClothes;
    }

    private BedClothes toBedClothes(String line) {
        String[] strings = line.split(",");
        return BedClothes.builder()
                .id(Long.valueOf(strings[0]))
                .color(strings[1])
                .price(Double.valueOf(strings[2]))
                .quantity(Integer.valueOf(strings[3]))
                .build();
    }
}
