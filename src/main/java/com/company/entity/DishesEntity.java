package com.company.entity;

import com.company.config.FileReader;
import com.company.domains.Dishes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DishesEntity implements BaseEntity<Dishes> {

    private final String dishesFile = "src/main/resources/dishes.csv";

    private final FileReader fileReader = new FileReader();

    @Override
    public List<Dishes> findAll() throws IOException {
        return readDishesFile();
    }

    public List<Dishes> readDishesFile() throws IOException {
        List<Dishes> dishesList = new ArrayList<>();
        List<String> strings = fileReader.readFile(dishesFile);
        strings.forEach(s -> dishesList.add(toDishes(s)));
        return dishesList;
    }

    private Dishes toDishes(String line) {
        String[] strings = line.split(",");
        return Dishes.builder()
                .id(Long.valueOf(strings[0]))
                .name(strings[1])
                .price(Double.valueOf(strings[2]))
                .quantity(Integer.valueOf(strings[3]))
                .build();
    }
}
