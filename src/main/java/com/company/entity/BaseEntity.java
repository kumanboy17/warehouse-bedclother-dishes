package com.company.entity;

import java.io.IOException;
import java.util.List;

public interface BaseEntity<T> {
    List<T> findAll() throws IOException;
}
