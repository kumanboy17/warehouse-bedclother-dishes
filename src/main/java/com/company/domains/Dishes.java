package com.company.domains;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Dishes {
    private Long id;
    private String name;
    private Double price;
    private Integer quantity;
}
