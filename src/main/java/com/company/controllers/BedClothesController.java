package com.company.controllers;

import com.company.domains.BedClothes;
import com.company.dto.DataDTO;
import com.company.dto.ResponseEntity;
import com.company.services.BedClothesService;
import com.company.utils.BaseUtils;

import java.util.List;

public class BedClothesController implements BaseController {

    private final BedClothesService bedClothesService = new BedClothesService();

    public void findByColor() {
        BaseUtils.print("Enter color: ");
        ResponseEntity<DataDTO<List<BedClothes>>> responseEntity = bedClothesService.findByColor(BaseUtils.readText());
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }

    @Override
    public void findAll(String sort) {
        ResponseEntity<DataDTO<List<BedClothes>>> responseEntity = bedClothesService.findAll(sort);
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }

    @Override
    public void findByID() {
        BaseUtils.print("Enter id: ");
        ResponseEntity<DataDTO<BedClothes>> responseEntity = bedClothesService.findByID(BaseUtils.readLong());
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }

    @Override
    public void findByPrice() {
        BaseUtils.print("Enter price: ");
        ResponseEntity<DataDTO<List<BedClothes>>> responseEntity = bedClothesService.findByPrice(BaseUtils.readDouble());
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }

    @Override
    public void findByQuantity() {
        BaseUtils.print("Enter quantity: ");
        ResponseEntity<DataDTO<List<BedClothes>>> responseEntity = bedClothesService.findByQuantity(BaseUtils.readInteger());
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }

    @Override
    public void filterByPrice() {
        BaseUtils.print("Enter min: ");
        Double min = BaseUtils.readDouble();
        BaseUtils.print("Enter max: ");
        Double max = BaseUtils.readDouble();
        ResponseEntity<DataDTO<List<BedClothes>>> responseEntity = bedClothesService.filterByPrice(min, max);
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }
}
