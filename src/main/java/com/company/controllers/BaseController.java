package com.company.controllers;

public interface BaseController {

    void findAll(String sort);

    void findByID();

    void findByPrice();

    void findByQuantity();
    void filterByPrice();
}
