package com.company.ui;

import com.company.controllers.BedClothesController;
import com.company.controllers.DishesController;
import com.company.utils.BaseUtils;

import java.util.Objects;

public class AppUI {
    private final DishesController dishesController = new DishesController();
    private final BedClothesController bedClothesController = new BedClothesController();

    public void run() {
        BaseUtils.println("\n\n-----> MENU: ");
        BaseUtils.println("1 -> Dishes");
        BaseUtils.println("2 -> Bedclothes");
        BaseUtils.println("q -> Exit");
        BaseUtils.print("-- Select operation: ");
        String operation = BaseUtils.readText();

        switch (operation) {
            case "1" -> dishes();
            case "2" -> bedClothes();
            case "q" -> System.exit(0);
            default -> BaseUtils.println("Wrong choice!");
        }
        run();
    }

    private String baseUI() {
        BaseUtils.println("2 -> Show all");
        BaseUtils.println("3 -> Find by id");
        BaseUtils.println("4 -> Find by price");
        BaseUtils.println("5 -> Find by quantity");
        BaseUtils.println("6 -> Filter by price");
        BaseUtils.println("0 -> Back");
        BaseUtils.print("-- Select operation: ");
        return BaseUtils.readText();
    }

    private void dishes() {
        BaseUtils.println("\n\n1 -> Find by name");
        switch (baseUI()) {
            case "1" -> dishesController.findByName();
            case "2" -> showAllDishes();
            case "3" -> dishesController.findByID();
            case "4" -> dishesController.findByPrice();
            case "5" -> dishesController.findByQuantity();
            case "6" -> dishesController.filterByPrice();
            case "0" -> run();
            default -> BaseUtils.println("Wrong choice!");
        }
        dishes();
    }

    private void bedClothes() {
        BaseUtils.println("\n\n1 -> Find by color");
        switch (baseUI()) {
            case "1" -> bedClothesController.findByColor();
            case "2" -> showAllBedClothes();
            case "3" -> bedClothesController.findByID();
            case "4" -> bedClothesController.findByPrice();
            case "5" -> bedClothesController.findByQuantity();
            case "6" -> bedClothesController.filterByPrice();
            case "0" -> run();
            default -> BaseUtils.println("Wrong choice!\n\n");
        }
        bedClothes();
    }

    private String commonUI() {
        BaseUtils.println("\n\n1 -> Sort by id");
        BaseUtils.println("2 -> Sort by name");
        BaseUtils.println("3 -> Sort by price");
        BaseUtils.println("4 -> Sort by quantity");
        BaseUtils.println("0 -> Back");

        BaseUtils.print("-- Select operation: ");
        return BaseUtils.readText();
    }

    private void showAllDishes() {
        String operation = commonUI();
        if (Objects.equals(operation, "0")) {
            dishes();
        }
        dishesController.findAll(operation);
        showAllDishes();
    }

    private void showAllBedClothes() {
        String operation = commonUI();
        if (Objects.equals(operation, "0")) {
            bedClothes();
        }
        bedClothesController.findAll(operation);
        showAllBedClothes();
    }
}