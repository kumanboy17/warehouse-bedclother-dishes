import com.company.domains.Dishes;
import com.company.dto.DataDTO;
import com.company.dto.ResponseEntity;
import com.company.services.DishesService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class DishesServiceTest {

    private final DishesService dishesService = new DishesService();

    @Test
    public void findByNameTest() {
        String name = "bowl";
        ResponseEntity<DataDTO<Dishes>> responseEntity = dishesService.findByName(name);
        Assertions.assertEquals(responseEntity.getStatus(), 200, "Find by name method is not passed!");
    }

    @Test
    public void findByAllTest() {
        String sort = "1";
        ResponseEntity<DataDTO<List<Dishes>>> all = dishesService.findAll(sort);
        Assertions.assertTrue(all.getData().isSuccess(), "Find all method is not passed!");
    }

    @Test
    public void findByIDTest() {
        Long id = 1L;
        ResponseEntity<DataDTO<Dishes>> responseEntity = dishesService.findByID(id);
        Assertions.assertTrue(responseEntity.getData().isSuccess(), "Find by id method is not passed!");
    }

    @Test
    public void findByPriceTest() {
        Double price = 20D;
        ResponseEntity<DataDTO<List<Dishes>>> all = dishesService.findByPrice(price);
        List<Dishes> list = all.getData().getBody().stream().filter(dishes -> dishes.getPrice().equals(price)).toList();
        Assertions.assertFalse(list.isEmpty(), "Find by price method is not passed!");
    }

    @Test
    public void findByQuantityTest() {
        Integer quantity = 1;
        ResponseEntity<DataDTO<List<Dishes>>> all = dishesService.findByQuantity(quantity);
        Assertions.assertFalse(all.getData().isSuccess(), "Find by quantity method is not passed!");
    }

    @Test
    public void filterByPriceTest() {
        Double max = 10D;
        Double min = 5D;
        ResponseEntity<DataDTO<List<Dishes>>> all = dishesService.filterByPrice(min, max);
        Assertions.assertTrue(all.getData().isSuccess(), "Filter by price method is not passed!!");
    }
}
