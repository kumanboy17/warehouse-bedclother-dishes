# Report

## Bed Linens and Dishes Warehouse



| Actions                      | Start Date | End Date    |
|------------------------------|------------|-------------|
| Introduce requirements       | 20.04.2023 | 23.04.2023  |
| Entity Design                | 23.04.2023 | 30.05.2023  |
| Data Access Layer            | 01.05.2023 | 05.05.2023  |
| Service Layer                | 06.05.2023 | 13.05.2023  |
| Controller Layer             | 15.05,2023 | 16.05.2023  |
| Creating Repository on Gitlab| 17.05,2023 | 17.05.2023  |
| Testing                      | 18.05,2023 | 20.05.2023  |   
| Fixing errors                | 20.05,2023 | 24.05.2023  |     
| Final Testing                | 25.05,2023 | 27.05.2023  |        
| Create & edit Readme & Report| 27.05,2023 | 01.06.2023  |  
| Preparing Presantation       | 01.06,2023 | 02.06.2023  |                     
| End Course Project           | 03.06,2023 | 03.06.2023  |                     


